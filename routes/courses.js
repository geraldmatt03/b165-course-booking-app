// [SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/courses');
	const auth = require('../auth');

	// destructure verify from auth
	const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] [POST] Routes
	route.post('/create', verify, verifyAdmin, (req, res) => {
		let data = req.body; 
		controller.createCourse(data).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] [GET] Routes
	route.get('/all', (req, res) => {
		controller.getAllCourse().then(outcome => {
			res.send(outcome);
		});
	});

	route.get('/:id', (req, res) => {
		let courseId = req.params.id;
		controller.getCourse(courseId).then(result => {
			res.send(result);
		});
	});

	route.get('/', (req, res) => {
		controller.getAllActiveCourse().then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] [PUT] Routes
	route.put('/:id', verify, verifyAdmin,  (req, res) => {
		let id = req.params.id;
		let details = req.body;

		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;

		if (cName !== '' && cDesc !== '' && cCost !== '') {
			controller.updateCourse(id, details).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send({message: 'Incorrect Input, Make sure all details are complete'});
		};
	}); 

	route.put('/:id/archive', (req, res) => {
		let courseId = req.params.id;

		controller.deactivateCourse(courseId).then(resultOfTheFunction => {
			res.send(resultOfTheFunction);
		});
	});

	route.put('/:id/reactivate', verify, verifyAdmin,  (req, res) => {
		let courseId = req.params.id;

		controller.reactivateCourse(courseId).then(result => {
			res.send(result);
		});
	});

// [SECTION] [DELETE] Routes
	route.delete('/:id', verify, verifyAdmin,  (req, res) => {
		let id = req.params.id;
		controller.deleteCourse(id).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Export Route System
	module.exports = route;