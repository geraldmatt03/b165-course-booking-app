// [SECTION] Dependencies and Packages
	const mongoose = require('mongoose');

// [SECTION] Schema / Document Blueprint
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Course Name is Required']
		},
		description: {
			type: String,
			required: [true, 'Course description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Course price is Required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User's ID is Required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});


// [SECTION] Model
	const Course = mongoose.model("Course", courseSchema);
	module.exports = Course;