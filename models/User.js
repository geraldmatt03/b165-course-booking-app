// [SECTION] Dependencies and Modules
	const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
	const userSchema = new mongoose.Schema({
		firstName:{
			type: String,
			required: [true, 'First Name is Required']
		},
		lastName:{
			type: String,
			required: [true, 'Last Name is Required']
		},
		email:{
			type: String,
			required: [true, 'Email is Required']
		},
		password:{
			type: String,
			required: [true, 'Password is Required']
		},
		gender:{
			type: String,
			required: [true, 'Gender is Required']
		},
		mobileNo:{
			type: String,
			required: [true, 'Mobile Number is Required']
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		enrollments:[
			{
				courseId: {
					type: String,
					required: [true, 'Course ID is Required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'Enrolled'
				}
			}
		]
	});

// [SECTION] Model
	const User = mongoose.model('User', userSchema);
	module.exports = User;