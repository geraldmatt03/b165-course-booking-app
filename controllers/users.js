// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth');

// [SECTION] Environment Setup
	dotenv.config();
	const salt = Number(process.env.SALT);

// [SECTION] Functionalities [CREATE]
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNo;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			gender: gendr,
			mobileNo: mobil
		});

		return newUser.save().then((user, rejected) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register a New Account'};
			};
		});
	};

	module.exports.loginUser = (req, res) => {
		console.log(req.body)

		User.findOne({email: req.body.email}).then(foundUser => {
			if (foundUser === null) {
				return res.send({message: "User Not Found"});
			} else {
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
				console.log(isPasswordCorrect);
				if (isPasswordCorrect) {
					return res.send({accessToken: auth.CreateAccessToken(foundUser)});
				} else {
					return res.send(false);
				}
			};
		}).catch(err => res.send(err));
	};

	module.exports.enroll = async (req, res) => {
		console.log(req.user.id);
		console.log(req.body.courseId);

		if (req.user.isAdmin) {
			return res.send({message: "Action Forbidden"});
		}

		let isUserUpdated = await User.findById(req.user.id).then(user => {
			let newEnrollment = {
				courseId: req.body.courseId
			};
			user.enrollments.push(newEnrollment);

			return user.save().then(user => true).catch(err => err.message)
		});

		if (isUserUpdated !== true) {
			return res.send({message: isUserUpdated});
		}
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			let enrollee = {
				userId: req.user.id
			};

			course.enrollees.push(enrollee);

			return course.save().then(course => true).catch(err => err.message)
		});
		if(isCourseUpdated !== true) {
			return res.send({message: isCourseUpdated});
		};

		if (isUserUpdated && isCourseUpdated) {
			return res.send({message: "Enrolled Successfully"});
		}
	};

// [SECTION] Functionalities [RETRIEVE]
	module.exports.getUserDetails = (req, res) => {
		console.log(req.user);
		User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err));
	};

	module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err));
	};
// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]