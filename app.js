// [SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');

// [SECTION] Server Setup
	const app = express();
	dotenv.config();
	app.use(express.json());
	app.use(cors())
	const secret = process.env.CONNECTION_STRING;
	const port = process.env.PORT;

// [SECTION] Application Routes
	app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);

// [SECTION] Database Connection
	mongoose.connect(secret);
	let connectStatus = mongoose.connection;
	connectStatus.on('open', () => console.log(`Database is connected`));

// [SECTION] Gateway Response
	app.get('/', (req, res) =>{
		res.send(`Welcome to Gerald's Basic Cycling Course`);
	});
	app.listen(port, () => console.log(`Server is running on port ${port}`));